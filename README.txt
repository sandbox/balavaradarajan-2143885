This module is intended to provide a list of all modules present in the site in 
a user friendly manner with all the relevant information.

All modules regardless of their status namely installed, uninstalled and 
disabled present in the site will be listed.

